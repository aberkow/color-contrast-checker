<?php
/*
inputs trigger a request to the proxy with the color values. 
those are received by the proxy and a request is made to the “real” api endpoint. 
when the request is successful it’s returned. 
the returned request is received by the js file
*/
// https://webaim.org/resources/contrastchecker/?fcolor=F000F0&bcolor=F5F5F5&api

$bcolor = $_GET['bcolor'];
$fcolor = $_GET['fcolor'];
$url = "https://webaim.org/resources/contrastchecker/?fcolor=$fcolor&bcolor=$bcolor&api";

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
$request = curl_exec($curl);

return $request;